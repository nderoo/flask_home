#!/bin/bash 

set -e 
echo "0"
# 1 : on récupère la clé
mkdir -p ~/.ssh # On crée le dossier SSH 
touch ~/.ssh/id_rsa # On crée le fichier qui contiendra la clé
echo -e "$AWS_KEY" > ~/.ssh/id_rsa # On insère la clé dans le fichier
chmod 600 ~/.ssh/id_rsa # On change les droits du fichier
echo "0.1"
# 2 : On change la configuration de ssh 
touch ~/.ssh/config # On crée un fichier de config 
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config # On désactive le Key checking
echo "0. 2"
# 3 : Connexion SSH 
ssh ec2-user@$AWS_IP 'bash -s' < ./scripts/updateAndRestart.sh
echo "0.3"