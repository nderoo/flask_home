#!/bin/bash 

set -e 
echo "1"
if [ "$(docker ps -q -f name=flask)" ]; then
    docker rm -f flask
    docker rmi -f $(docker images -q)
fi
echo "2"
if [ ! "$(docker ps -q -f name=flask)" ]; then
    docker run -p 8080:8080 -d --name flask eultartuffe/flask:1.0
fi